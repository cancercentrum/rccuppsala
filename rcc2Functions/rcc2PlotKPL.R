
# # # # # #
# T E S T #
# # # # # #

if (FALSE) {
  group <- paste0("Grupp ",toupper(letters))
  outcome <- runif(10*length(group))
  outcome <- 1*(outcome>0.33) + 1*(outcome>0.66)
  tab <- matrix(outcome,nrow=length(group))
  rownames(tab) <- group
  colnames(tab) <- 1:ncol(tab)
  
  tab[1,1] <- NA
  tab[15,5] <- NA
  
  rcc2PlotKPL(
    tab=tab
  )
}

# # # # # #
# # # # # #
# # # # # #

rcc2PlotKPL <- 
  function(
    tab=NULL,
    group_maxchars=NULL,
    possible_values=NULL,
    legend_show=FALSE,
    legend_ncol=NULL,
    col=NULL,
    col_kpl=TRUE,
    grid=TRUE,
    title=NULL,
    subtitle=NULL,
    text_cex=1,
    point_cex=5,
    showpoint=FALSE,
    description=NULL,
    description_addnumbers=TRUE,
    showtotalscore=FALSE,
    showtotalscore_sort=TRUE
  ) {
    
    suppressMessages(require(RColorBrewer))
    
    if (is.null(possible_values)) {
      possible_values <- min(tab,na.rm=TRUE):max(tab,na.rm=TRUE)
    }
    
    tab[1:nrow(tab),1:ncol(tab)] <- sapply(tab,function(x){replace(x,!(!is.na(x) & x%in%possible_values),NA)})
    
    if (showtotalscore) {
      tab_totalscore <- 100*rowSums(tab,na.rm=TRUE)/(max(possible_values,na.rm=TRUE)*rowSums(!is.na(tab)))
      if (showtotalscore_sort) {
        temp_order <- order(-tab_totalscore,rownames(tab))
        tab <- tab[temp_order,]
        tab_totalscore <- tab_totalscore[temp_order]
      }
    }
    
    if (is.null(legend_ncol)) {
      legend_ncol <- ifelse(length(possible_values)<=3,length(possible_values),ifelse(length(possible_values)==4,2,3))
    }
    legend_nrow <- ceiling(length(possible_values)/legend_ncol)
    
    # Colors
    if (is.null(col)) {
      if (col_kpl) {
        col <- c("#ed5564","#f9b032","#97cf48",rep("#b2b2b2",max(0,length(possible_values)-3)))
      } else {
        col <- brewer.pal(max(3,length(possible_values)),"RdYlGn")
        if (length(possible_values)==2) {
          col <- c(head(col,1),tail(col,1))
        } else if (length(possible_values)==1) {
          col <- tail(col,1)
        }
      }
    }

    col_text <- "#FFFFFF"

    # Shorten group names
    if (!is.null(group_maxchars)) {
      rownames(tab)[nchar(rownames(tab))>group_maxchars] <- paste0(substr(rownames(tab)[nchar(rownames(tab))>group_maxchars],1,group_maxchars-3),"...")
    }
    
    # x-axis label and ticks
    barheight <- 1
    barheight_factor <- 1.4
    x_max <- ncol(tab)
    x_lim <- c(0,x_max)
    
    y_bp <- 0.5*barheight_factor*barheight+barheight*barheight_factor*(0:(nrow(tab)-1))
    y_lim <- c(0,barheight*barheight_factor*nrow(tab))

    # Change margins
    linchheight <- strheight("X","inch",cex=text_cex)
    linchwidth <- strwidth("X","inch",cex=text_cex)
    linch_label <- 3*linchwidth+max(strwidth(rownames(tab),"inch",cex=text_cex),na.rm=TRUE)
    
    par(
      mai=c(
        (0+legend_show*legend_nrow)*linchheight+(length(description)+3)*1.1*linchheight,
        linch_label,
        (1+2*(!is.null(title))+2*(!is.null(title) & !is.null(subtitle)))*linchheight,
        (2+3*(showtotalscore))*linchwidth
        ),
      bg="#ffffff",
      xpd=TRUE
      )
    
    # Empty plot
    plot(
      x=x_lim,
      y=y_lim,
      axes=FALSE,
      type="n",
      xlab="",
      ylab=""
    )
    
    luserheight <- strheight("X","user",cex=text_cex)
    luserwidth <- strwidth("X","user",cex=text_cex)
    
    pos0x <- grconvertX(x=0,from="nfc",to="user")
    pos1x <- grconvertX(x=1,from="nfc",to="user")
    pos0y <- grconvertY(y=0,from="nfc",to="user")
    pos1y <- grconvertY(y=1,from="nfc",to="user")

    y_n_label <- grconvertY(grconvertY(y=y_lim[2],from="user",to="inches")+1*linchheight,from="inches",to="user")

    # Grid
    if (grid) {
      for (i in seq(y_lim[1],y_lim[2],barheight_factor*barheight)) {
        lines(x=c(0,x_lim[2]),y=rep(i,2),lwd=1,col="#bdbdbd")
      }
      for (i in seq(x_lim[1],x_lim[2],1)) {
        lines(x=rep(i,2),y=c(0,y_lim[2]),lwd=1,col="#bdbdbd")
      }
    }
    
    # Title
    if (!is.null(title)) {
      if (!is.null(subtitle)) {
        text(x=pos0x,y=y_n_label+0.4*(pos1y-y_n_label),labels=subtitle,pos=4,cex=text_cex,offset=1)
        text(x=pos0x,y=y_n_label+0.4*(pos1y-y_n_label)+1.8*strheight("","user",cex=text_cex),labels=title,pos=4,cex=1.5*text_cex,offset=1)
      } else {
        text(x=pos0x,y=0.5*sum(y_n_label,pos1y),labels=title,pos=4,cex=1.5*text_cex,offset=1)  
      }
    }
    
    # Axis label
    text(x=1:ncol(tab)-0.5,y=y_n_label,labels=1:ncol(tab),cex=text_cex,font=2)
    
    # Plot
    for (i in 1:nrow(tab)) {
      
      for (j in 1:ncol(tab)) {
        
        temp_point <- tab[nrow(tab)+1-i,j]
        temp_col <- col[which(possible_values==temp_point)]
        
        points(x=j-0.5,y=y_bp[i],pch=16,cex=point_cex,col=temp_col)
        
        if (showpoint) {
          text(x=j-0.5,y=y_bp[i],labels=temp_point,cex=0.8*text_cex,font=2,col=col_text)
        }
        
      }
      
      # Score
      if (showtotalscore) {
        text(x=x_lim[2],y=y_bp[i],labels=paste(round(tab_totalscore[length(tab_totalscore)+1-i]),"%"),cex=text_cex,font=2,pos=4)
      }

    }
    
    # Legend
    if (legend_show) {
      legend(x=0.5*x_max,y=pos0y,legend=possible_values,col=col,pch=15,pt.cex=1.75,bty="n",cex=0.8*text_cex,xjust=0.5,yjust=0,y.intersp=1,ncol=legend_ncol,text.width=max(strwidth(possible_values)))
    }
    
    # Group labels
    text(x=-luserwidth,y=y_bp,labels=rev(rownames(tab)),pos=2,cex=text_cex)
    
    # Description
    if (!is.null(description)) {
      for (i in 1:length(description)) {
        text(x=pos0x,y=y_lim[1]-(1+i)*1.25*luserheight,labels=paste0(ifelse(description_addnumbers,paste0(i,". "),""),description[i]),pos=4,cex=0.8*text_cex)
      }
    }
    
  }
