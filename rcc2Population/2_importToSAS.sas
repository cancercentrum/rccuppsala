
%let path = R:\Kvalitetsregister\_Bitbucket\rccUppsala\rcc2Population;

libname out "&path\utdata";

FILENAME infile "&path\utdata\popmort_county.txt" ENCODING = "UTF-8";

PROC IMPORT OUT = indata
	DATAFILE = infile
	DBMS = DLM REPLACE; 
	DELIMITER = "|";
	GETNAMES = YES;
RUN;

data out.popmort_county;
	set indata;
run;

FILENAME infile "&path\utdata\popmort_region.txt" ENCODING = "UTF-8";

PROC IMPORT OUT = indata
	DATAFILE = infile
	DBMS = DLM REPLACE; 
	DELIMITER = "|";
	GETNAMES = YES;
RUN;

data out.popmort_region;
	set indata;
run;

FILENAME infile "&path\utdata\popmort.txt" ENCODING = "UTF-8";

PROC IMPORT OUT = indata
	DATAFILE = infile
	DBMS = DLM REPLACE; 
	DELIMITER = "|";
	GETNAMES = YES;
RUN;

data out.popmort;
	set indata;
run;
