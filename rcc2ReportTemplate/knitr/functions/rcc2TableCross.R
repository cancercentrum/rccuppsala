
# # # # # #
# T E S T #
# # # # # #

if (FALSE) {
  rowVar <- 
    sample(
      paste0(
        "Grupp ",
        rep(toupper(letters[1:3]), each = 1000)
      )
    )
  colVar <- 
    sample(
      paste0(
        "Grupp ",
        rep(toupper(letters[1:3]), each = 1000)
      )
    )

  rcc2TableCross(
    rowVar = rowVar,
    colVar = colVar,
    #pctMargin = 2,
    outputNumbers = TRUE,
    outputLatex = TRUE
  )
}

# # # # # #
# # # # # #
# # # # # #

rcc2TableCross <-
  function(
    rowVar = NULL,
    colVar = NULL,
    data = NULL,
    pctMargin = 1,
    outputNumbers = FALSE,
    outputTotalLabel = "Totalt",
    outputLatex = FALSE,
    nDec = 1
  ) {
    
    if (!is.null(data)) {
      rowVar <- data[,rowVar]
      colVar <- data[,colVar]
    }
    
    # Table
  	temp_table 	<- 
  	  table(
  	    rowVar,
  	    colVar,
  	    useNA = "ifany"
      )
  	
  	column_names <- c(
  	  dimnames(temp_table)[[2]],
  	  outputTotalLabel
  	 )
  	row_names	<- c(
  	  dimnames(temp_table)[[1]],
  	  outputTotalLabel
  	 )
  
  	temp_table <- as.data.frame.matrix(addmargins(temp_table))
  
  	# Determine denominator
  	if (pctMargin == 2) {
      denominator <- 
  	    matrix(
  	      rep(
  	        as.matrix(temp_table["Sum",]),
  	        nrow(temp_table)
          ),
  	      nrow = nrow(temp_table),
  	      byrow = TRUE
        )
  	} else {
      denominator <- 
        matrix(
          rep(
            as.matrix(temp_table[,"Sum"]),
            ncol(temp_table)
          ),
          ncol = ncol(temp_table),
          byrow = FALSE
        )
  	}

  	# Percent
  	temp_percent 	<- 100 * (temp_table / denominator)
  
  	# Finalize table
  	if (!(pctMargin %in% 1:2)) {
  	  
  		temp_final_table 	<- temp_table
  		colnames(temp_final_table) 	<- column_names
  		
  	} else {
  	  
  	  temp_final_table <- 
  	    matrix(
  	      "",
  	      nrow = nrow(temp_table),
  	      ncol = 2 * ncol(temp_table)
  	    )
  	  
  	  temp_final_table[, seq(1, ncol(temp_final_table), 2)] <- as.matrix(temp_table)
  	  temp_final_table[, seq(2, ncol(temp_final_table), 2)] <- 
  	    apply(
  	      temp_percent,
  	      2,
  	      function(x) {
  	        temp_percent_apply <- 
  	          paste0(
  	            ifelse(outputNumbers, "", "("),
  	            trimws(format(round(x, digits = nDec), nsmall = nDec)),
  	            ifelse(outputNumbers, "", ")")
  	          )
  	        gsub(x = temp_percent_apply, pattern = "NaN", replacement = "-")
  	      }
  	    )
  	  
  	  if (outputLatex) {
  	    temp_n <- temp_final_table[, seq(1, ncol(temp_final_table), 2)]
  	    temp_p <- temp_final_table[, seq(2, ncol(temp_final_table), 2)]
  	    if (pctMargin != 2) temp_p[, ncol(temp_p)] <- ""
  	    temp_p_nchar <- nchar(temp_p)
  	    temp_pmaxchar <- 3 + 2 * (!outputNumbers) + 1 * (nDec > 0) + nDec
  	    temp_enspace <- 
  	      matrix(
  	        "",
  	        nrow = nrow(temp_n),
  	        ncol = ncol(temp_n)
  	      )
  	    temp_enspace[temp_p_nchar == temp_pmaxchar] <- "\\enspace"
  	    temp_enspace[temp_p_nchar == temp_pmaxchar - 1] <- "\\enspace\\enspace"
  	    temp_enspace[temp_p_nchar == temp_pmaxchar - 2] <- "\\enspace\\enspace\\enspace"
  	    for (i in 1:ncol(temp_n)) {
  	      temp_n[, i] <- 
  	        paste0(
  	          temp_n[, i],
  	          temp_enspace[, i],
  	          temp_p[, i]
  	         )
  	    }
  	    temp_final_table <- temp_n
  	    colnames(temp_final_table) 	<- column_names
  	  } else {
  	    if (pctMargin != 2) temp_final_table <- temp_final_table[, -ncol(temp_final_table)]
  	    colnames(temp_final_table) <- 
  	      paste(
  	        rep(column_names, each = 2)[1:(2 * length(column_names) - 1 * (pctMargin != 2))],
  	        c("(n)","(%)")
  	       )
  	  }
  	}
  	
  	rownames(temp_final_table) 	<- row_names
  	
  	return(temp_final_table)
  
  }
