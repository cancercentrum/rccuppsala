
setwd(dirname(rstudioapi::getActiveDocumentContext()$path))

library(tidyverse)

load("./indata/organisationsenheter-df.RData")

df <- df %>%
  
  # Variabelnamn till lowercase
  dplyr::rename_all(iconv) %>% 
  dplyr::rename_all(tolower) %>% 
  
  # factor till character
  dplyr::mutate_if(is.factor, as.character) %>% 
  
  # Variabelformat
  readr::type_convert() %>%
  
  # Encoding
  dplyr::mutate_if(is.character, iconv) %>% 
  
  # integer till numeric
  dplyr::mutate_if(is.integer, as.numeric) 

df <- 
  subset(
    df,
    topposcode %in% 1:6 & 
      !grepl("[[:alpha:]]",as.character(topposcode)) & 
      !grepl("[[:alpha:]]",as.character(parentposcode))
  )

df$regionCode <- as.numeric(as.character(df$topposcode))

df$hospitalCode <- as.numeric(as.character(df$parentposcode))
df$hospitalName <- as.character(df$parentposname)

df <- 
  subset(
    df,
    !is.na(hospitalCode) & nchar(hospitalCode) > 1 & 
      !is.na(hospitalName) & hospitalName != "",
    select = c(
      "regionCode", 
      "hospitalCode", 
      "hospitalName",
      "posenabled"
    )
  )

df <- df[order(df$hospitalCode, -df$posenabled),]
df <- subset(df,!duplicated(data.frame(hospitalCode)))

dfHospitals <- 
  subset(
    df,
    select = -posenabled
  )

save(
  dfHospitals,
  file = "./utdata/dfHospitals.RData"
)

write.table(
  x = dfHospitals,
  file = "./utdata/dfHospitals.txt",
  sep = "|",
  row.names = FALSE,
  col.names = TRUE
)
